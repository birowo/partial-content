package main

import (
	"errors"
	"os"
	"sync"

	"github.com/valyala/fasthttp"
)

func atoi(str []byte) (num int64, err error) {
	strLen := len(str)
	if strLen == 0 {
		err = errors.New("invalid number")
		return
	}
	for i := 0; i < strLen; i++ {
		if str[i] < '0' || str[i] > '9' {
			err = errors.New("invalid number")
			return
		}
		num *= 10
		num += int64(str[i] - '0')
	}
	return
}
func itoa(num int64) (str string) {
	for num > 0 {
		str = string(byte(num%10)+'0') + str
		num = num / 10
	}
	if str == "" {
		str = "0"
	}
	return
}
func partialContent(bufSz int64, f1l3 *os.File, ct []byte) fasthttp.RequestHandler {
	pool := sync.Pool{
		New: func() (ret interface{}) {
			ret = make([]byte, bufSz)
			return
		},
	}
	info, _ := f1l3.Stat()
	size := info.Size()
	szStr := "/" + itoa(size)
	notSatisfy := []byte("bytes *" + szStr)
	return func(ctx *fasthttp.RequestCtx) {
		r4ng3 := ctx.Request.Header.PeekBytes([]byte("Range"))
		rangeLen := len(r4ng3)
		i := 6
		for i < rangeLen && r4ng3[i] != '-' {
			i++
		}
		if i < rangeLen {
			bgn, err1 := atoi(r4ng3[6:i])
			end, err2 := atoi(r4ng3[i+1:])
			var cl int64
			if err1 == nil && err2 == nil && end < size && bgn < end {
				cl = end - bgn + 1
				if bufSz < cl {
					cl = bufSz
					end = bgn + cl - 1
				}
			} else if err1 == nil && bgn < size {
				cl = size - bgn
				if bufSz < cl {
					cl = bufSz
				}
				end = bgn + cl - 1
			} else if err2 == nil && end < size {
				bgn = size - end
				if bufSz < end {
					cl = bufSz
					end = bgn + cl - 1
				} else {
					cl = end
					end = size - 1
				}
			}
			ctx.SetStatusCode(fasthttp.StatusPartialContent)
			ctx.SetContentTypeBytes(ct)
			ctx.Response.Header.SetBytesK([]byte("Content-Range"), "bytes "+itoa(bgn)+"-"+itoa(end)+szStr)
			ctx.Response.Header.SetBytesK([]byte("Content-Length"), itoa(cl))
			buf := pool.Get().([]byte)
			f1l3.ReadAt(buf[:cl], bgn)
			ctx.Write(buf[:cl])
			pool.Put(buf)
		} else {
			ctx.SetStatusCode(fasthttp.StatusRequestedRangeNotSatisfiable)
			ctx.Response.Header.SetBytesKV([]byte("Content-Range"), notSatisfy)
		}
	}
}
func root(ctx *fasthttp.RequestCtx) {
	ctx.SetContentTypeBytes([]byte("text/html"))
	ctx.Write([]byte(`
<html><body>
<video width="400" autoplay controls src="/video"></video>
</body></html>
	`))
}

type Route map[string]fasthttp.RequestHandler

func router(route Route) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		if hndlr, ok := route[string(ctx.RequestURI())]; ok {
			hndlr(ctx)
		}
	}
}
func main() {
	bufferSize := int64(64 * 1024) // 64 Kbytes
	vidFile, err := os.Open("video.webm")
	if err != nil {
		println(err.Error())
		return
	}
	defer vidFile.Close()
	contentType := []byte("video/webm")

	fasthttp.ListenAndServe(":8080", router(Route{
		"/":      root,
		"/video": partialContent(bufferSize, vidFile, contentType),
	}))
}
